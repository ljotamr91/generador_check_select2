
/*crear un bucle dentro de otro bucle para generar subselects cuando cliquemos en 2.2 
  se generen subcheckbox con el numero del .5
*/




// cuando clice en el boton generar del html
$(document).on("click","#generar",function(){

    // declaramos la variable entrada que contendra el valor del imput.
    var entrada = $("#entradaNumerica").val();

    /* creamos un bucle para determinar cuantas veces se incrementara el valor de i.
       i es igual a 1 por que ya tenemos pintado lo que queremos 1 vez. 
       cuando i sea menor o igual al valor que introducimos cortamos el bucle.
       de lo contrario incrementa el valor de i en +1. 
    */
    for (i = 1 ; i<=entrada ; i++) { 
        
        // añade el valor option al final de la id selecgenerado del html tantas veces como el valor i.
        $("#selectGenerado").append("<option value=>select "+ i + "</option> ");

        // añade el valor input al final de la id checkgenerado del html tantas veces como el valor i.
         $("#checkGenerados").append( i + "   <input type='checkbox'> ");


    }
});